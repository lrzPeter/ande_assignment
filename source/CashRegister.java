import java.util.Scanner;

public class CashRegister
{
   public static void main(String[] args)
   {
      String s="", c="";
      double balance=0,totalCost=0;

      Scanner in = new Scanner(System.in);
      
      System.out.println("Welcome to use cash register.");
      System.out.print("Please enter cash register's float:");
      while(true){
    	  s = in.nextLine();
    	  
          try {
        	  balance = Double.parseDouble(s);
        	  break;
          } catch (Exception e) {
    		  System.out.println("please input float number:");
    	  }
      }
      
      while(true){
          System.out.println("if your wish to exit input (0), or process a transaction input(1):");
          s = in.nextLine();
          if(s.equalsIgnoreCase("0")){
        	  break;
          }
          String order="============= order ===============\n";
          while(true){
        	  System.out.print("Please enter the item's name:");
              s = in.nextLine();
              order+=s +"\t";
              
              System.out.print("Please enter thee item's cost:");
              c = in.nextLine();
              order+=c+"\t";
              double cc=0;
              try {
                  cc=Double.parseDouble(c);
              } catch (Exception e) {
        		  System.out.println("please input number:");
        	  }
              
              Transaction trans = new Transaction(s, cc);
              order+=trans.getCost()+"\n";
            		  
              totalCost+=trans.getCost();
              
              System.out.println("if all items have been entered? (yes/no)");
              s = in.nextLine();
              
              if(s.equalsIgnoreCase("yes")){
            	  break;
              }
          }
          
          order+="total cost: "+ totalCost +"\n";

          System.out.println("Total cost is "+totalCost);

          System.out.print("Please enter the cash amount tendered:");
          s = in.nextLine();
          
          order+="cash amount: "+ s +"\n";
          
          c = Double.toString(Double.parseDouble(s) - totalCost);
          
          order+="change amount: "+ c+"\n";
          System.out.println("Amount of change required = " + c);
          
          System.out.println();
          System.out.println(order);
          
          c = Double.toString(balance + totalCost);
      }
      
      System.out.println("Balance of the Cash Registerrrr:"+c);

	  System.out.println("exit");
   }
}
